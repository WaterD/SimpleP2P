﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace SimpleP2PExample
{
    class Program
    {
        static void Main(string[] args)
        {
            if (Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName).Count() <= 1)
            {
                for (int i = 0; i < 4; i++)
                {
                    Process.Start("SimpleP2PExample.exe");
                }
            }

            new Program().Run();
        }

        public void Run()
        {
            Console.WriteLine("Starting the simple P2P demo.");

            var peer = new Peer("Peer(" + Guid.NewGuid() + ")");
            var peerThread = new Thread(peer.Run) { IsBackground = true };
            peerThread.Start();

            //wait for the server to start up.
            Thread.Sleep(1000);

            while (true)
            {
                Console.Write("Enter Something: ");
                string tmp = Console.ReadLine();

                if (tmp == "") break;

                peer.Channel.Ping(peer.Id, tmp);
            }

            peer.Stop();
            peerThread.Join();
        }
    }

    [ServiceContract(CallbackContract = typeof(IPing))]
    public interface IPing
    {
        [OperationContract(IsOneWay = true)]
        void Ping(string sender, string message);
    }

    public class PingImplementation : IPing
    {
        public void Ping(string sender, string message)
        {
            Console.WriteLine("{0} says: {1}", sender, message);
        }
    }

    public class Peer
    {
        public string Id { get; private set; }

        public IPing Channel;
        public IPing Host;

        public Peer(string id)
        {
            Id = id;
        }
        public void StartService()
        {
            var binding = new NetPeerTcpBinding();
            binding.Security.Mode = SecurityMode.None;

            var endpoint = new ServiceEndpoint(
                ContractDescription.GetContract(typeof(IPing)), 
                binding, 
                new EndpointAddress("net.p2p://110.15.101.133"));

            Host = new PingImplementation();

            _factory = new DuplexChannelFactory<IPing>(
                new InstanceContext(Host),
                endpoint);

            var channel = _factory.CreateChannel();

            ((ICommunicationObject)channel).Open();

            // wait until after the channel is open to allow access.
            Channel = channel;
        }
        private DuplexChannelFactory<IPing> _factory;

        public void StopService()
        {
            ((ICommunicationObject)Channel).Close();
            if (_factory != null)
                _factory.Close();
        }

        private readonly AutoResetEvent _stopFlag = new AutoResetEvent(false);

        public void Run()
        {
            Console.WriteLine("[ Starting Service ]");
            StartService();

            Console.WriteLine("[ Service Started ]");
            _stopFlag.WaitOne();

            Console.WriteLine("[ Stopping Service ]");
            StopService();

            Console.WriteLine("[ Service Stopped ]");
        }

        public void Stop()
        {
            _stopFlag.Set();
        }
    }
}
